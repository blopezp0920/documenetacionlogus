# Crear una nueva página en WordPress

Para crear nuevas páginas en WordPress es sencillo y se hace en simples pasos

## Primer paso

Vamos al menú que está en la parte izquierda de la administración y damos click en la parte de "Páginas"

[![Opci-n-Paginas.png](https://i.postimg.cc/YqkY267N/Opci-n-Paginas.png)](https://postimg.cc/V5GdDbd5)

## Segundo paso

Al estar en el apartado, procedemos a dar click en "Añadir nueva"

![Imagen](https://i.postimg.cc/MHPYtZWV/Apartado-De-Pagina.png)

## Tercer paso

Procedemos a darle un título a la página que vamos a crear

![Imagen](https://i.postimg.cc/dVT9mKgZ/Panel-Creacion-Pagina.png)

## Cuarto paso (opcional)

Podemos editar el nombre del link de la nueva página, por lo general se deja el nombre que le asigna a la página

![Imagen](https://i.postimg.cc/CL2NqKrV/SlugURL.png)

## Quinto paso

Damos click en "Editar con Elementor"

![Imagen](https://i.postimg.cc/VsD4zgjv/Click-Elementor-Pagina.png)

## Sexto paso

Aparecerá la página por defecto

![Imagen](https://i.postimg.cc/8CQmZ0VK/Aparece-Pagina-Por-Defecto.png)

## Séptimo paso 

Damos click en los ajustes de la página para configurar la margen

![Imagen](https://i.postimg.cc/fLs5Rg2t/Configuracion-Elementor-Pagina.png)

## Octavo paso

Seleccionamos "Full width template"

![Imagen](https://i.postimg.cc/tTzkZtwr/Opcion-Estructura-Pagina.png)

## Noveno paso

Este menú deberá desaparecer

![Imagen](https://i.postimg.cc/RVhdhtRR/Menu-Que-Se-Debe-Quitar.png)

## Décimo paso

La página deberá quedar de esta manera

![Imagen](https://i.postimg.cc/MG7bNm6W/Pagina-Final-Sin-Edicion-De-Elementor.png)
