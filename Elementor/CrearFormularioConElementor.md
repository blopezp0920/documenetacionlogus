## Crear formulario con Elementor

Esta función es muy importante y fácil de utilizar porque nos puede ayudar al momento de que necesitemos hacer alguna encuesta, concurso, etc. Además se configurará para que se mande correos de respuesta tanto al usuario como a los administradores

[![Alt text](https://img.youtube.com/vi/Ilbn4oGmkYI/0.jpg)](https://www.youtube.com/watch?v=Ilbn4oGmkYI)

## Verificar respuestas de los formularios en la plataforma

Esta función les permite a los administradores verificar las respuestas, archivos y cualquier tipo de información de las respuestas que hayan dado los usuarios en los formularios unicamente elaboradas con Elementor.

[![Alt text](https://img.youtube.com/vi/KCXF1jtCBGg/0.jpg)](https://www.youtube.com/watch?v=KCXF1jtCBGg)
