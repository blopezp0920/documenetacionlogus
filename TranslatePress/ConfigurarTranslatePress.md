## Configurar TranslatePress

Este plugin nos será de mucha ayuda para los diferentes idiomas que podamos tener en la plataforma, acutalmente la plataforma cuenta con idiomas en español y en portugués. Si se quieren agregar más idiomas, se tendrá que pagar licencia para ello. Este es el link del plugin y con el mismo link se puede comprar: https://translatepress.com/ 

## Vídeo de configuración para otros idiomas o para traducir alguna paǵina de la plataforma

[![Alt text](https://img.youtube.com/vi/WKgGb9itj1c/0.jpg)](https://www.youtube.com/watch?v=WKgGb9itj1c)
