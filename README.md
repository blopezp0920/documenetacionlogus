# Documenetación Logus

El presente repositorio trata sobre la documentación respecto a la plataforma Logus, la cual está desarrollada por el sistema de gestión de contenidos [WordPress](https://wordpress.org/)

## ¿Qué son los plugins en WordPress?

Un plugin de WordPress es un fragmento de código que se conecta al sitio web de WordPress. En pocas palabras, es una extensión al sitio que modifica y mejora las funciones principales de WordPress.
- [ ] [Saber más](https://www.hostinger.co/tutoriales/que-es-plugin-wordpress?ppc_campaign=google_search_generic_hosting_all&bidkw=defaultkeyword&lo=1003652&gclid=Cj0KCQjwxb2XBhDBARIsAOjDZ36o0XRyCXt8Bm0ZG6ZCa3pWOXFl1YWFzILLHLdwREJQoH3Aif3ENXAaAjeSEALw_wcB)

## ¿Con cuántos plugins cuenta la plataforma?

Actualmente cuenta con 43 plugins, 40 activos y 3 inactivos. Los 43 que se encuentran activos son necesarios para el funcionamiento de la plataforma y los 3 inactivos son un plus que le darán a la plataforma en un futuro dependiendo de las necesidades.

## Lista de plugins activos

- [ ] [Advanced Custom Fields](#advanced-custom-fields)
- [ ] [Akismet Anti Spam](#akismet-anti-spam)
- [ ] [All in One WP Migration](#all-in-one-wp-migration)
- [ ] [Author Avatars List/Block](#author-avatars-list-block)
- [ ] [Automatic Translate Addon For TranslatePress](#automatic-translate-addon-for-translatepress)
- [ ] [BlogLentor for Elementor](#bloglentor-for-elementor)
- [ ] [BP Profile Search](#bp-profile-search)
- [ ] [BuddyPress](#buddypress)
- [ ] [BuddyPress Global Search](#buddypress-global-search)
- [ ] [Elementor](#elementor)
- [ ] [Exclusive Addons Elementor](#exclusive-addons-elementor)
- [ ] [File Upload Types](#file-upload-types)
- [ ] [Frontend Admin](#frontend-admin)
- [ ] [Grimlock](#grimlock)
- [ ] [Grimlock Animate](#grimlock-animate)
- [ ] [Grimlock for Author Avatars List/Block](#grimlock-for-author-avatars-list-block)
- [ ] [Grimlock for bbPress](#grimlock-for-bbpress)
- [ ] [Grimlock for BuddyPress](#grimlock-for-buddypress)
- [ ] [Grimlock for Elementor](#grimlock-for-elementor)
- [ ] [If Menu - Visibility control for menus](#if-menu-visibility-control-for-menus)
- [ ] [Kirki Customizer Framework](#kirki-customizer-framework)
- [ ] [Member Swipe for BuddyPress](#member-swipe-for-buddypress)
- [ ] [Members](#members)
- [ ] [PDF Embedder](#pdf-embedder)
- [ ] [Post SMTP](#post-smtp)
- [ ] [PublishPress Permissions](#publishpress-permissions)
- [ ] [Search & Filter](#search-filter)
- [ ] [TaxoPress](#taxopress)
- [ ] [The Events Calendar](#the-events-calendar)
- [ ] [TranslatePress - Multilingual](#translatepress-multilengual)
- [ ] [Ultimate Member](#ultimate-member)
- [ ] [User Role Editor](#user-role-editor)
- [ ] [WP fail2ban](#wp-fail2ban)
- [ ] [WPForms](#wpforms)
- [ ] [WPForms Drip](#wpforms-drip)
- [ ] [WPForms Drop Uploader](#wpforms-drop-uploader)
- [ ] [WPForms Post Submissions](#wpforms-post-submissions)

## Lista de plugins inactivos

- [ ] [BuddyPress Docs](#buddypress-docs)
- [ ] [BuddyPress Message Attachment](#buddypress-message-attachment)
- [ ] [wpForo](#wpforo)

# Advanced Custom Fields

Advanced Custom Fields convierte los sitios de WordPress en un sistema totalmente adaptable de gestión de contenidos ofreciéndote las herramientas que precisas para sacar más partido de los datos. 

- Añade campos bajo demanda. ¡Nuestro maquetador te permite añadir campos de manera rápida y sencilla a las pantallas de edición de WP con solo hacer clic en unos pocos botones!

- Añade campos en cualquier parte. Los campos se pueden añadir por todo WP incluido entradas, usuarios, taxonomías, medios, comentarios y ¡Páginas de opciones personalizadas!

- Muestra los campos donde quieras. Carga y muestra tus valores de campos personalizados en cualquier fichero de plantilla del tema con nuestra funcionalidad amigable para desarrolladores ¡Es muy sencillo!

[Ver documentación](https://www.advancedcustomfields.com/resources/?utm_source=wordpress.org&utm_medium=free%20plugin%20listing&utm_campaign=ACF%20Website)

# Akismet Anti Spam

Akismet revisa tus comentarios y envíos en el formulario de contacto frente nuestra base de datos global de spam para prevenir que se publique en tu sitio contenido malicioso. Puedes revisar los comentarios spam que captura en la pantalla «Comentarios» de la administración de tu blog.

Las principales características que incluye Akismet:

- Comprueba automáticamente todos los comentarios y filtra los que parecen spam.
- Cada comentario tiene un historial, por lo tanto puedes facilmente ver qué comentarios fueron cazados o eliminados por Akismet y cuáles fueron gestionado por un moderador.
- Las URLs se muestran en el cuerpo del comentario para desvelar enlaces ocultos o engañosos.
- Los moderadores pueden ver el número de comentarios aprobados por cada usuario.
- Una función de descarte que bloquea totalmente el peor spam, ahorrandote espacio en el disco y acelerando las actualizaciones de tu sitio.

[Ver documentación](https://akismet.com/)

# All in One WP Migration

Introducido en 2013 y utilizado por más de 60 millones de sitios web, All-in-One WP Migration es sin duda uno de los plugins más confiables y utilizados de WordPress para mover sitios web con absoluta facilidad.

Intencionalmente construido con el usuario final en mente, All-in-One WP Migration viene cargado con funciones amigables para principiantes que le permiten migrar su sitio web de WordPress con poco o ningún conocimiento técnico o experiencia.

### Avalado por el gobierno y grandes empresas:

Muchos clientes empresariales, organizaciones gubernamentales y universidades utilizan,
aman y confían en All-in-One WP Migration. Estos son algunos: Boeing, NASA, VW, IBM, Universidad de Harvard, Universidad de Stanford, Lego, P&G, Automattic, Estado de California, Estado de Hawái.
Esta amplia adopción y uso de All-in-One WP Migration demuestra cuán seguro, confiable y adaptable es el plugin para casi cualquier necesidad de migración de webs.

[Ver documentación](https://help.servmask.com/knowledgebase/all-in-one-wp-migration-user-guide/)

# Author Avatars List Block

Este plugin facilita la visualización de listas de avatares de usuarios, agrupados por roles de usuario, en su sitio (multiusuario). También te permite insertar avatares individuales para los usuarios del blog o cualquier dirección de correo electrónico en un post o página - ideal para mostrar una imagen de alguien de quien estás hablando.

# Automatic Translate Addon For TranslatePress

Traduce con un solo clic cualquier contenido de la página. Sólo traducirá cadenas de texto plano, lo que significa que si una cadena contiene HTML o un carácter especial, se omitirá de las traducciones automáticas.

Puedes traducir automáticamente un número ilimitado de caracteres de forma gratuita sin necesidad de una clave API. Sólo tienes que instalar este complemento y hacer clic en el botón de traducción para traducir cualquier contenido de la página.

Después de las traducciones automáticas, también puede editar manualmente cualquier cadena traducida por la máquina utilizando TranslatePress.

[Ver documentación](https://translatepress.com/?utm_source=wp.org&utm_medium=tp-description-page&utm_campaign=TPFree)

# BlogLentor for Elementor

BlogLentor es un plugin basado en Elementor fácil de usar para crear hermosas entradas de blog, listado de entradas, deslizador de entradas y carrusel de entradas en pocos segundos.

Cada ajuste es visualmente editable en BlogLentor. Podrás cambiar el diseño del post con los ajustes de estilo. Si no tienes suficientes habilidades de diseño, no te preocupes, muchos diseños creativos y disposiciones están listos para tus necesidades.

[Ver documentación](https://wordpress.org/plugins/bloglentor-for-elementor/)

# BP Profile Search

BP Profile Search es un plugin de búsqueda de miembros y directorios de miembros para BuddyPress. Proporciona:

- Un constructor de formularios para construir los formularios de búsqueda de miembros.
- El shortcode [bps_directory] para personalizar el directorio de miembros de BuddyPress, o para construir directorios de miembros adicionales
- Cada formulario de búsqueda tiene un directorio de destino. Cuando ejecutas una búsqueda, el directorio de destino del formulario se filtra de acuerdo a tu búsqueda.

[Ver documentación](https://es.wordpress.org/plugins/bp-profile-search/)

# BuddyPress

BuddyPress ayuda a que los maquetadores de sitios y los desarrolladores añadan características de comunidad a sus webs. Viene con una API de compatibilidad de temas muy robusta, que hace todo lo posible para que la visualización y el aspecto de cada página de contenido de BuddyPress vaya bien con casi cualquier tema para WordPress. Es probable que tengas que ajustar algunos estilos por tu cuenta para que todo se vea impecable.

[Ver documentación](https://codex.buddypress.org/participate-and-contribute/)

# BuddyPress Global Search

Deja que tus miembros busquen en todos los componentes de BuddyPress, junto con las páginas y entradas y los tipos de entradas personalizados que elijas, todo en una barra de búsqueda unificada con un desplegable de resultados en vivo.

[Ver documentación](https://wpprotools.io/plugins/buddypress-global-search/)

# Elementor

Elementor es un plugin de creación de páginas que reemplaza el editor básico de WordPress con un editor de tipo arrastrar y soltar, por lo que podrás crear sitios web con una gran terminación, y trabajar en vivo sin tener que guardar cambios a cada instante, ir a la vista previa y volver al editor.

[Ver documentación](https://elementor.com/?utm_source=wp-plugins&utm_campaign=author-uri&utm_medium=wp-dash&utm_content=elementor-pro)

# Exclusive Addons Elementor

Exclusive Addons es uno de los mejores complementos de Elementor que le trae el diseño más moderno y de moda a su experiencia de construcción de páginas Elementor. Los complementos Elementor le da la capacidad de construir una experiencia sofisticada sitio web con 100 + elementos altamente personalizables y creativos y extensiones.

Una colección de más de 800 bloques prefabricados le ayudará a construir sus sitios web en menos tiempo y sin necesidad de codificación.

[Ver documentación](https://devscred.com/)

# File Upload Types

¿Quieres que tu sitio web de WordPress acepte subidas de tus usuarios para más tipos de archivo y que suban archivos libremente? Hemos creado el plugin File Upload Types para que cualquiera pueda añadir fácilmente soporte para cualquier tipo de archivo con cualquier extensión o tipo MIME.

[Ver documentación](https://wordpress.org/plugins/file-upload-types/)

# Frontend Admin

Este impresionante plugin le permite mostrar fácilmente los formularios de administración frontend en su sitio para que sus clientes puedan editar fácilmente el contenido por sí mismos desde el frontend. Puedes crear impresionantes formularios con nuestro constructor de formularios para permitir a los usuarios guardar metadatos personalizados en páginas, publicaciones, usuarios y más. A continuación, utiliza nuestro bloque Gutenberg o shortcode para mostrar fácilmente el formulario a tus usuarios.

[Ver documentación](https://wordpress.org/plugins/frontend-admin/)

# If Menu Visibility control for menus

Controle qué elementos del menú ven los visitantes de su sitio, con reglas de visibilidad. He aquí algunos ejemplos:

- Muestra un elemento de menú solo si el usuario está conectado.
- Ocultar los menús si el dispositivo es móvil.
- Muestra los menús a los administradores y editores.
- Ocultar los enlaces de inicio de sesión o registro para los usuarios registrados.
- Mostrar los menús sólo para los clientes con membresía activa.
- Una vez habilitado el plugin, cada elemento del menú tendrá una nueva opción "Cambiar la visibilidad del elemento del menú" que permitirá seleccionar las reglas de visibilidad.

[Ver documentación](https://layered.market/plugins/if-menu)

# Kirki Customizer Framework

Con más de 30 controles personalizados, que van desde controles deslizantes simples a controles tipográficos complejos con integración con Google Fonts y características como la generación automática de CSS y scripts postMessage, Kirki facilita muchísimo el desarrollo de temas.

[Ver documentación](https://kirki.org/?utm_source=repo&utm_medium=description&utm_campaign=kirki)

# Member Swipe for BuddyPress

Deslízate por tus miembros de BuddyPress con un movimiento de tu dedo en tus dispositivos táctiles, o con un clic de tu ratón en el escritorio.

# Members

Un plugin de gestión de usuarios y perfiles que te ofrece el control de los permisos de tu sitio. Este plugin te permite editar tus perfiles y sus capacidades, clonar perfiles existentes, asignar varios perfiles por usuario, bloquear contenido de entradas, o incluso hacer que tu sitio sea completamente privado.

# PDF Embedder

Incrusta PDFs directamente en tus publicaciones y páginas, con versatilidad en el ancho y la altura. Sin obligación de uso de servicios de terceras partes. Compatible con Gutenberg, el editor de WordPress

[Ver documentación](https://wp-pdf.com/)

# Post SMTP 

¿No es fiable el correo electrónico? Post SMTP es el primer y único plugin SMTP para WordPress que implementa OAuth 2.0 para Gmail, Hotmail y Yahoo Mail. La configuración es muy sencilla con el asistente de configuración y el comprobador de puertos integrado. ¡Disfruta de una entrega sin preocupaciones, incluso si tu contraseña cambia!

[Ver documentación](https://wordpress.org/plugins/post-smtp/)

# PublishPress Permissions

Permisos de contenido avanzados pero accesibles. Otorga a los usuarios o grupos perfiles específicos para cada tipo de contenido. Permite o bloquea el acceso a entradas o términos específicos.

[Ver documentación](https://publishpress.com/permissions/?partners=48)

# Search & Filter

Search & Filter es un simple plugin de búsqueda y filtrado para WordPress - es un avance de la caja de búsqueda de WordPress.

Puedes buscar por Categoría, Etiqueta, Taxonomía Personalizada, Tipo de Publicación, Fecha de Publicación o cualquier combinación de estos fácilmente para realmente refinar tus búsquedas - elimina la caja de búsqueda y úsala como un sistema de filtrado para tus publicaciones y páginas. Los campos pueden mostrarse como desplegables, casillas de verificación, botones de radio o selecciones múltiples.

[Ver documentación](https://searchandfilter.com/)

# TaxoPress

TaxoPress te permite crear y gestionar etiquetas, categorías y todos los términos de la taxonomía de WordPress.

[Ver documentación](https://taxopress.com/)

# The Events Calendar

Crea y gestiona fácilmente un calendario de eventos en tu sitio de WordPress con el plugin gratuito The Events Calendar. Tanto si tus eventos son presenciales como virtuales, este plugin de calendario para WordPress cuenta con funciones profesionales respaldadas por nuestro equipo de desarrolladores y diseñadores de primer nivel.

[Ver documentación](https://theeventscalendar.com/knowledgebase/guide/the-events-calendar/?utm_source=theeventscalendar&utm_medium=plugin&utm_campaign=tec-onboarding&utm_content=tec-read-me)

# TranslatePress Multilingual

TranslatePress es un plugin de traducción de WordPress que puede utilizar todo el mundo.

La interfaz te permite traducir fácilmente toda la página a la vez, lo que incluye las salidas de los shortcodes, los formularios y los constructores de páginas. También funciona con WooCommerce directamente.

Construido siguiendo la filosofía de WordPress, TranslatePress – Multilingual es un plugin de traducción con licencia GPL y autoalojado, lo que significa que serás propietario de todas tus traducciones, por siempre. Es la manera más rápida de crear un sitio bilingüe o multilingüe.

[Ver documentación](https://translatepress.com/?utm_source=wp.org&utm_medium=tp-description-page&utm_campaign=TPFree)

# Ultimate member

Ultimate Member es el plugin de perfil de usuario y membresía #1 para WordPress. El plugin hace que sea muy fácil para los usuarios registrarse y convertirse en miembros de su sitio web. El plugin le permite añadir hermosos perfiles de usuario a su sitio y es perfecto para crear comunidades online avanzadas y sitios de membresía. Ligero y altamente extensible, Ultimate Member le permitirá crear casi cualquier tipo de sitio donde los usuarios puedan unirse y hacerse miembros con absoluta facilidad.

[Ver documentación](https://ultimatemember.com/)


# User Role Editor

El plugin de WordPress User Role Editor le permite cambiar los roles y capacidades de los usuarios fácilmente.
Sólo tienes que activar las casillas de verificación de las capacidades que deseas añadir al rol seleccionado y hacer clic en el botón "Actualizar" para guardar los cambios.

[Ver documentación](https://www.role-editor.com/)

# WP fail2ban

WPf2b viene con tres filtros fail2ban: wordpress-hard.conf, wordpress-soft.conf, y wordpress-extra.conf. Estos están diseñados para permitir una división entre el baneo inmediato (hard) y el enfoque tradicional más elegante (soft), con reglas extra para configuraciones personalizadas.

[Ver documentación](https://es.wordpress.org/plugins/wp-fail2ban/)

# WPForms

WPForms es un constructor de formularios de WordPress de arrastrar y soltar que es a la vez fácil y potente. WPForms le permite crear hermosos formularios de contacto, formularios de suscripción, formularios de pago y otro tipo de formularios para su sitio en minutos, sin tener que contratar a un desarrollador. WPForms Drip, WPForms Drop Uploader y WPForms Post Submissions, son complementos de WPForms.

[Ver documentación](https://wpengine.com/solution-center/wpforms/#:~:text=WPForms%20is%20a%20drag%20and,having%20to%20hire%20a%20developer)

## Autores
Show your appreciation to those who have contributed to the project.
